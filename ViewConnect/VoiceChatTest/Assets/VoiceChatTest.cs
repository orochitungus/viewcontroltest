﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonobitEngine;

public class VoiceChatTest : MonobitEngine.MonoBehaviour 
{
	/// <summary>
	/// 自身がルームに入室を試みたかどうかのフラグ
	/// </summary>
	bool isJoinOrCreateRoom = false;

	/// <summary>
	/// 自身のプレイヤーキャラクターが生成されたかどうかのフラグ
	/// </summary>
	bool isMakeMyPlayer = false;

	// Use this for initialization
	void Start () 
	{
		// MUNサーバーに接続する
		MonobitNetwork.autoJoinLobby = true;
		MonobitNetwork.ConnectServer("VoiceChatTest_v1.0");
	}
	
	// Update is called once per frame
	void Update () 
	{
		// 未接続状態なら何もしない
		if(!MonobitNetwork.isConnect)
		{
			return;
		}
		// ルーム入室処理未実行であれば、ルームの作成or入室を許可する
		if(!isJoinOrCreateRoom)
		{
			MonobitNetwork.JoinOrCreateRoom("VoiceChatTest", new RoomSettings(), null);
			isJoinOrCreateRoom = true;
			return;
		}
		// ルーム入室済みでキャラクタ未生成であれば、キャラクターを生成する
		if(MonobitNetwork.inRoom && !isMakeMyPlayer)
		{
			MonobitNetwork.Instantiate("Cube", Vector3.zero, Quaternion.identity, 0);
			isMakeMyPlayer = true;
			return;
		}
	}
}
