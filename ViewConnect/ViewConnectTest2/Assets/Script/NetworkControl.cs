﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonobitEngine;

public class NetWorkControl : MonobitEngine.MonoBehaviour
{
	/// <summary>
	/// ルーム名
	/// </summary>
	private string RoomName = "";

	/// <summary>
	/// 表示するオブジェクト
	/// </summary>
	private GameObject ViewObject = null;

	/// <summary>
	/// マスターフラグ
	/// </summary>
	[SerializeField]
	private Toggle MasterFlag;

	/// <summary>
	/// ウインドウ表示フラグ
	/// </summary>
	private bool bDisplayWindow = false;

	/// <summary>
	/// サーバー接続失敗フラグ
	/// </summary>
	private bool bConnectFailed = false;

	/// <summary>
	/// サーバ途中切断フラグ
	/// </summary>
	private static bool bDisconnect = false;

	/// <summary>
	/// ウィンドウ表示用メソッド
	/// </summary>
	/// <param name="windowId"></param>
	void WindowControl(int windowId)
	{
		// GUIスタイル設定
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleCenter;
		GUIStyleState stylestate = new GUIStyleState();
		stylestate.textColor = Color.white;
		style.normal = stylestate;

		// 途中切断時の表示
		if (bDisconnect)
		{
			GUILayout.Label("途中切断しました。\n再接続しますか？", style);
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("はい", GUILayout.Width(50)))
			{
				// もう一度接続処理を実行する
				MonobitNetwork.ConnectServer("Bearpocalypse_v1.0");
				bDisconnect = false;
				bDisplayWindow = false;
			}
			if (GUILayout.Button("いいえ", GUILayout.Width(50)))
			{
				// シーンをリロードし、初期化する
				Application.LoadLevel(Application.loadedLevel);
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			return;
		}

		// 接続失敗時の表示
		if (bConnectFailed)
		{
			GUILayout.Label("接続に失敗しました。\n再接続しますか？", style);
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("はい", GUILayout.Width(50)))
			{
				// もう一度接続処理を実行する
				MonobitNetwork.ConnectServer("Bearpocalypse_v1.0");
				bConnectFailed = false;
				bDisplayWindow = false;
			}
			if (GUILayout.Button("いいえ", GUILayout.Width(50)))
			{
				// オフラインモードで起動する
				MonobitNetwork.offline = true;
				bConnectFailed = false;
				bDisplayWindow = false;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			return;
		}
	}

	// Use this for initialization
	void Start()
	{
		// デフォルトロビーへの自動入室を許可する
		MonobitNetwork.autoJoinLobby = true;

		// MUNサーバーに接続する
		MonobitNetwork.ConnectServer("ViewConnectTest_v1.0");

		// FPSを固定する
		Application.targetFrameRate = 30;

	}

	private void Update()
	{
		// MUNサーバーに接続しており、かつルームに入室している場合
		if (MonobitNetwork.isConnect && MonobitNetwork.inRoom)
		{
			// 表示されていない場合に表示(スレイブ・マスター双方でとりあえず描画するので両方に出す）
			if (ViewObject == null)
			{
				ViewObject = MonobitNetwork.Instantiate("MainUnit", Vector3.zero, Quaternion.identity, 0);
			}
		}

		if (MasterFlag.isOn)
		{
			SavingParameter.IsMaster = true;
		}
		else
		{
			SavingParameter.IsMaster = false;
		}
	}

	// OnGUI is called once per frame
	void OnGUI()
	{
		// サーバ接続状況に応じて、ウィンドウを表示する
		if (bDisplayWindow)
		{
			GUILayout.Window(0, new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 80), WindowControl, "Caution");
		}

		// デフォルトボタンと被らないように段下げを行う
		GUILayout.Space(24);

		// MUNサーバーに接続している場合
		if (MonobitNetwork.isConnect)
		{
			// ボタン入力でサーバから切断＆シーンリセット
			if (GUILayout.Button("Disconnect", GUILayout.Width(150)))
			{
				// 正常動作のため、bDisconnect を true にして、GUIウィンドウ表示をキャンセルする
				bDisconnect = true;

				// サーバから切断する
				MonobitNetwork.DisconnectServer();

				// シーンをリロードする
				Application.LoadLevel(Application.loadedLevelName);
			}

			// ルームに入室している場合
			if (MonobitNetwork.inRoom)
			{
				// ボタン入力でルームから退室
				if (GUILayout.Button("Leave Room", GUILayout.Width(150)))
				{
					MonobitNetwork.LeaveRoom();
				}
			}

			// ルームに入室していない場合
			if (!MonobitNetwork.inRoom)
			{
				GUILayout.BeginHorizontal();

				// ルーム名の入力
				GUILayout.Label("RoomName : ");
				RoomName = GUILayout.TextField(RoomName, GUILayout.Width(200));

				// ボタン入力でルーム作成
				if (GUILayout.Button("Create Room", GUILayout.Width(150)))
				{
					MonobitNetwork.CreateRoom(RoomName);
				}

				GUILayout.EndHorizontal();

				// ルーム一覧から選択式で入室する
				foreach (RoomData room in MonobitNetwork.GetRoomData())
				{
					if (GUILayout.Button("Enter Room : " + room.name + "(" + room.playerCount + "/" + ((room.maxPlayers == 0) ? "-" : room.maxPlayers.ToString()) + ")"))
					{
						MonobitNetwork.JoinRoom(room.name);
					}
				}
			}
		}
	}

	/// <summary>
	/// サーバー接続失敗コールバック
	/// </summary>
	/// <param name="parameters"></param>
	public void OnConnectToServerFailed(object parameters)
	{
		Debug.Log("OnConnectToServerFailed : StatusCode = " + parameters);
		bConnectFailed = true;
		bDisplayWindow = true;
	}

	/// <summary>
	/// 途中切断コールバック
	/// </summary>
	public void OnDisconnectedFromServer()
	{
		Debug.Log("OnDisconnectedFromServer");
		if (bDisconnect == false)
		{
			bDisconnect = true;
			bDisplayWindow = true;
		}
		else
		{
			bDisconnect = false;
		}
	}


}
