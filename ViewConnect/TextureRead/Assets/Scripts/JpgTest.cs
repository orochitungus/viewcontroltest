﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JpgTest : MonoBehaviour 
{
	[SerializeField]
	private Renderer Quad;

	[SerializeField]
	private Texture2D Tex;

	// Use this for initialization
	void Start () 
	{
		// テクスチャーをjpgに変換
		byte[] jpg = Tex.EncodeToJPG();
		// 変換したテクスチャーの入れ子を作成
		Texture2D texture = new Texture2D(640,480, TextureFormat.RGBA32, false);
		// Quadに張り付ける
		texture.LoadImage(jpg);
		Quad.material.mainTexture = texture;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
