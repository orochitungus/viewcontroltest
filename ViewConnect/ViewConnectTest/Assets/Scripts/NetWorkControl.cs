﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonobitEngine;
using MonobitEngine.VoiceChat;

public class NetWorkControl : MonobitEngine.MonoBehaviour 
{
	/// <summary>
	/// ルーム名
	/// </summary>
	private string RoomName = "";

	/// <summary>
	/// 表示するオブジェクト
	/// </summary>
	private GameObject ViewObject = null;

	/// <summary>
	/// マスターフラグ
	/// </summary>
	[SerializeField]
	private Toggle MasterFlag;

	/// <summary>
	/// ウインドウ表示フラグ
	/// </summary>
	private bool bDisplayWindow = false;

	/// <summary>
	/// サーバー接続失敗フラグ
	/// </summary>
	private bool bConnectFailed = false;

	/// <summary>
	/// サーバ途中切断フラグ
	/// </summary>
	private static bool bDisconnect = false;

	/// <summary>
	/// ルーム名
	/// </summary>
	private string roomName = "";

	/// <summary>
	/// ルーム内のプレイヤーに対するボイスチャット送信可否設定.
	/// </summary>
	private Dictionary<MonobitPlayer, Int32> vcPlayerInfo = new Dictionary<MonobitPlayer, int>();

	/// <summary>
	/// 自身が所有するボイスアクターのMonobitViewコンポーネント
	/// </summary>
	private MonobitVoice myVoice = null;

	/// <summary>
	/// ボイスチャット送信可否設定の定数.
	/// </summary>
	private enum EnableVC
	{
		ENABLE = 0,         /**< 有効. */
		DISABLE = 1,        /**< 無効. */
	}


	/// <summary>
	/// ウィンドウ表示用メソッド
	/// </summary>
	/// <param name="windowId"></param>
	void WindowControl(int windowId)
	{
		// GUIスタイル設定
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleCenter;
		GUIStyleState stylestate = new GUIStyleState();
		stylestate.textColor = Color.white;
		style.normal = stylestate;
		style.fontSize = 30;

		// 途中切断時の表示
		if (bDisconnect)
		{
			GUILayout.Label("途中切断しました。\n再接続しますか？", style);
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("はい",style, GUILayout.Width(50)))
			{
				// もう一度接続処理を実行する
				MonobitNetwork.ConnectServer("Bearpocalypse_v1.0");
				bDisconnect = false;
				bDisplayWindow = false;
			}
			if (GUILayout.Button("いいえ",style, GUILayout.Width(50)))
			{
				// シーンをリロードし、初期化する
				Application.LoadLevel(Application.loadedLevel);
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			return;
		}

		// 接続失敗時の表示
		if (bConnectFailed)
		{
			GUILayout.Label("接続に失敗しました。\n再接続しますか？", style);
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("はい",style, GUILayout.Width(50)))
			{
				// もう一度接続処理を実行する
				MonobitNetwork.ConnectServer("Bearpocalypse_v1.0");
				bConnectFailed = false;
				bDisplayWindow = false;
			}
			if (GUILayout.Button("いいえ",style, GUILayout.Width(50)))
			{
				// オフラインモードで起動する
				MonobitNetwork.offline = true;
				bConnectFailed = false;
				bDisplayWindow = false;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			return;
		}
	}

	// Use this for initialization
	void Start () 
	{
		// デフォルトロビーへの自動入室を許可する
		MonobitNetwork.autoJoinLobby = true;

		// MUNサーバーに接続する
		MonobitNetwork.ConnectServer("ViewConnectTest_v1.0");

		// FPSを固定する
		Application.targetFrameRate = 30;
		
	}

	private void Update()
	{
		// MUNサーバーに接続しており、かつルームに入室している場合
		if (MonobitNetwork.isConnect && MonobitNetwork.inRoom)
		{
			// 表示されていない場合に表示(スレイブ・マスター双方でとりあえず描画するので両方に出す）
			if(ViewObject == null)
			{
				ViewObject = MonobitNetwork.Instantiate("MainUnit", Vector3.zero, Quaternion.identity,0);
			}
		}

		if(MasterFlag.isOn)
		{
			SavingParameter.IsMaster = true;
		}
		else
		{
			SavingParameter.IsMaster = false;
		}
	}

	// OnGUI is called once per frame
	void OnGUI () 
	{
		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		// フォントサイズ確定
		myButtonStyle.fontSize = 30;

		// サーバ接続状況に応じて、ウィンドウを表示する
		if (bDisplayWindow)
		{
			GUILayout.Window(0, new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 80), WindowControl, "Caution");
		}

		// デフォルトボタンと被らないように段下げを行う
		GUILayout.Space(24);

		// MUNサーバーに接続している場合
		if(MonobitNetwork.isConnect)
		{
			// ボタン入力でサーバから切断＆シーンリセット
			if (GUILayout.Button("Disconnect", myButtonStyle, GUILayout.Width(300), GUILayout.Height(100)))
			{
				// 正常動作のため、bDisconnect を true にして、GUIウィンドウ表示をキャンセルする
				bDisconnect = true;

				// サーバから切断する
				MonobitNetwork.DisconnectServer();

				// シーンをリロードする
				Application.LoadLevel(Application.loadedLevelName);
			}

			// ルームに入室している場合
			if (MonobitNetwork.inRoom)
			{
				// ボタン入力でルームから退室
				if (GUILayout.Button("Leave Room", myButtonStyle, GUILayout.Width(300), GUILayout.Height(100)))
				{
					MonobitNetwork.LeaveRoom();
				}

				if (myVoice != null)
				{
					// 送信タイプの設定
					GUILayout.BeginHorizontal();
					GUILayout.Label("VoiceChat Send Type : ");
					Int32 streamType = myVoice.SendStreamType == StreamType.BROADCAST ? 0 : 1;
					myVoice.SendStreamType = (StreamType)GUILayout.Toolbar(streamType, new string[] { "broadcast", "multicast" });
					GUILayout.EndHorizontal();

					// マルチキャスト送信の場合の、ボイスチャットの送信可否設定
					if (myVoice.SendStreamType == StreamType.MULTICAST)
					{
						List<MonobitPlayer> playerList = new List<MonobitPlayer>(vcPlayerInfo.Keys);
						List<MonobitPlayer> vcTargets = new List<MonobitPlayer>();
						foreach (MonobitPlayer player in playerList)
						{
							// GUI による送信可否の切替
							GUILayout.BeginHorizontal();
							GUILayout.Label("PlayerName : " + player.name + " ");
							GUILayout.Label("Send Permission: ");
							vcPlayerInfo[player] = GUILayout.Toolbar(vcPlayerInfo[player], new string[] { "Allow", "Deny" });
							GUILayout.EndHorizontal();
							// ボイスチャットの送信可のプレイヤー情報を登録する
							if (vcPlayerInfo[player] == (Int32)EnableVC.ENABLE)
							{
								vcTargets.Add(player);
							}
						}

						// ボイスチャットの送信可否設定を反映させる
						myVoice.SetMulticastTarget(vcTargets.ToArray());
					}
				}
			}

			// ルームに入室していない場合
			if (!MonobitNetwork.inRoom)
			{
				GUILayout.BeginHorizontal();

				// ルーム名の入力
				GUILayout.Label("RoomName : ", myButtonStyle);
				RoomName = GUILayout.TextField(RoomName, myButtonStyle, GUILayout.Width(300), GUILayout.Height(100));

				// ボタン入力でルーム作成
				if (GUILayout.Button("Create Room", myButtonStyle, GUILayout.Width(300), GUILayout.Height(100)))
				{
					MonobitNetwork.CreateRoom(RoomName);
				}

				GUILayout.EndHorizontal();

				// ルーム一覧から選択式で入室する
				foreach (RoomData room in MonobitNetwork.GetRoomData())
				{
					if (GUILayout.Button("Enter Room : " + room.name + "(" + room.playerCount + "/" + ((room.maxPlayers == 0) ? "-" : room.maxPlayers.ToString()) + ")", myButtonStyle, GUILayout.Width(300), GUILayout.Height(150)))
					{
						MonobitNetwork.JoinRoom(room.name);
					}
				}
			}
		}
	}

	/// <summary>
	/// サーバー接続失敗コールバック
	/// </summary>
	/// <param name="parameters"></param>
	public void OnConnectToServerFailed(object parameters)
	{
		Debug.Log("OnConnectToServerFailed : StatusCode = " + parameters);
		bConnectFailed = true;
		bDisplayWindow = true;
	}

	/// <summary>
	/// 途中切断コールバック
	/// </summary>
	public void OnDisconnectedFromServer()
	{
		Debug.Log("OnDisconnectedFromServer");
		if (bDisconnect == false)
		{
			bDisconnect = true;
			bDisplayWindow = true;
		}
		else
		{
			bDisconnect = false;
		}
	}

	/// <summary>
	/// 自身がルーム入室に成功したときの処理
	/// </summary>
	public void OnJoinedRoom()
	{
		vcPlayerInfo.Clear();
		vcPlayerInfo.Add(MonobitNetwork.player, (Int32)EnableVC.DISABLE);

		foreach (MonobitPlayer player in MonobitNetwork.otherPlayersList)
		{
			vcPlayerInfo.Add(player, (Int32)EnableVC.ENABLE);
		}

		GameObject go = MonobitNetwork.Instantiate("VoiceActor", Vector3.zero, Quaternion.identity, 0);
		myVoice = go.GetComponent<MonobitVoice>();
		if (myVoice != null)
		{
#if UNITY_5_1 || UNITY_5_2 || UNITY_5_3_OR_NEWER
			UnityEngine.Debug.Assert(myVoice.SetMicrophoneErrorHandler(OnMicrophoneError));
			UnityEngine.Debug.Assert(myVoice.SetMicrophoneRestartHandler(OnMicrophoneRestart));
#else
	            myVoice.SetMicrophoneErrorHandler(OnMicrophoneError);
	            myVoice.SetMicrophoneRestartHandler(OnMicrophoneRestart);
#endif
		}
	}

	/// <summary>
	/// 誰かがルームにログインしたときの処理
	/// </summary>
	/// <param name="newPlayer"></param>
	public void OnOtherPlayerConnected(MonobitPlayer newPlayer)
	{
		if (!vcPlayerInfo.ContainsKey(newPlayer))
		{
			vcPlayerInfo.Add(newPlayer, (Int32)EnableVC.ENABLE);
		}
	}

	/// <summary>
	/// 誰かがルームからログアウトしたときの処理
	/// </summary>
	/// <param name="otherPlayer"></param>
	public virtual void OnOtherPlayerDisconnected(MonobitPlayer otherPlayer)
	{
		if (vcPlayerInfo.ContainsKey(otherPlayer))
		{
			vcPlayerInfo.Remove(otherPlayer);
		}
	}

	/// <summary>
	/// マイクのエラーハンドリング用デリゲート
	/// </summary>
	/// <returns>
	/// true : 内部にてStopCaptureを実行しループを抜けます。
	/// false: StopCaptureを実行せずにループを抜けます。
	/// </returns>
	public bool OnMicrophoneError()
	{
		UnityEngine.Debug.LogError("Error: Microphone Error !!!");
		return true;
	}

	/// <summary>
	/// マイクのリスタート用デリゲート
	/// </summary>
	/// <remarks>
	/// 呼び出された時点ではすでにStopCaptureされています。
	/// </remarks>
	public void OnMicrophoneRestart()
	{
		UnityEngine.Debug.LogWarning("Info: Microphone Restart !!!");
	}
}
