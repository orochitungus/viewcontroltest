﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnityExample;
using MonobitEngine;

public class TestSceneController : MonobitEngine.MonoBehaviour 
{
	/// <summary>
	/// Set the name of the device to use.
	/// </summary>
	[SerializeField, TooltipAttribute ("Set the name of the device to use.")]
	public string requestedDeviceName = null;

	/// <summary>
	/// Set the width of WebCamTexture.
	/// </summary>
	[SerializeField, TooltipAttribute ("Set the width of WebCamTexture.")]
	public int requestedWidth = 640;

	/// <summary>
	/// Set the height of WebCamTexture.
	/// </summary>
	[SerializeField, TooltipAttribute ("Set the height of WebCamTexture.")]
	public int requestedHeight = 480;

	/// <summary>
	/// Set FPS of WebCamTexture.
	/// </summary>
	[SerializeField, TooltipAttribute ("Set FPS of WebCamTexture.")]
	public int requestedFPS = 30;

	/// <summary>
	/// Set whether to use the front facing camera.
	/// </summary>
	[SerializeField, TooltipAttribute ("Set whether to use the front facing camera.")]
	public bool requestedIsFrontFacing = false;

	/// <summary>
	/// The webcam texture.
	/// </summary>
	WebCamTexture webCamTexture;

	/// <summary>
	/// The webcam device.
	/// </summary>
	WebCamDevice webCamDevice;

	/// <summary>
	/// The rgba mat.
	/// </summary>
	Mat rgbaMat;

	/// <summary>
	/// The colors.
	/// </summary>
	Color32[] colors;

	/// <summary>
	/// The texture.
	/// </summary>
	Texture2D texture;

	/// <summary>
	/// Indicates whether this instance is waiting for initialization to complete.
	/// </summary>
	bool isInitWaiting = false;

	/// <summary>
	/// Indicates whether this instance has been initialized.
	/// </summary>
	bool hasInitDone = false;

	[SerializeField]
	private Text DebugText;

	Texture2D takenPhoto;

	[SerializeField]
	private Renderer Quad;


	private bool Send = false;

	/// <summary>
	/// Initializes webcam texture.
	/// </summary>
	private void Initialize()
	{
		if (isInitWaiting)
			return;

#if UNITY_ANDROID && !UNITY_EDITOR
            // Set the requestedFPS parameter to avoid the problem of the WebCamTexture image becoming low light on some Android devices (e.g. Google Pixel, Pixel2).
            // https://forum.unity.com/threads/android-webcamtexture-in-low-light-only-some-models.520656/
            // https://forum.unity.com/threads/released-opencv-for-unity.277080/page-33#post-3445178
            if (requestedIsFrontFacing) {
                int rearCameraFPS = requestedFPS;
                requestedFPS = 15;
                StartCoroutine (_Initialize ());
                requestedFPS = rearCameraFPS;
            } else {
                StartCoroutine (_Initialize ());
            }
#else
		StartCoroutine(_Initialize());
#endif
	}

	// Use this for initialization
	void Start () 
	{
		Initialize();
		takenPhoto = new Texture2D(requestedWidth, requestedHeight, TextureFormat.RGB24, false);
	}

	/// <summary>
	/// Initializes webcam texture by coroutine.
	/// </summary>
	private IEnumerator _Initialize()
	{
		if (hasInitDone)
			Dispose();

		isInitWaiting = true;

		// Creates the camera
		if (!String.IsNullOrEmpty(requestedDeviceName))
		{
			int requestedDeviceIndex = -1;
			if (Int32.TryParse(requestedDeviceName, out requestedDeviceIndex))
			{
				if (requestedDeviceIndex >= 0 && requestedDeviceIndex < WebCamTexture.devices.Length)
				{
					webCamDevice = WebCamTexture.devices[requestedDeviceIndex];
					webCamTexture = new WebCamTexture(webCamDevice.name, requestedWidth, requestedHeight, requestedFPS);
				}
			}
			else
			{
				for (int cameraIndex = 0; cameraIndex < WebCamTexture.devices.Length; cameraIndex++)
				{
					if (WebCamTexture.devices[cameraIndex].name == requestedDeviceName)
					{
						webCamDevice = WebCamTexture.devices[cameraIndex];
						webCamTexture = new WebCamTexture(webCamDevice.name, requestedWidth, requestedHeight, requestedFPS);
						break;
					}
				}
			}
			if (webCamTexture == null)
				Debug.Log("Cannot find camera device " + requestedDeviceName + ".");
		}

		if (webCamTexture == null)
		{
			// Checks how many and which cameras are available on the device
			for (int cameraIndex = 0; cameraIndex < WebCamTexture.devices.Length; cameraIndex++)
			{
				if (WebCamTexture.devices[cameraIndex].isFrontFacing == requestedIsFrontFacing)
				{
					webCamDevice = WebCamTexture.devices[cameraIndex];
					webCamTexture = new WebCamTexture(webCamDevice.name, requestedWidth, requestedHeight, requestedFPS);
					break;
				}
			}
		}

		if (webCamTexture == null)
		{
			if (WebCamTexture.devices.Length > 0)
			{
				webCamDevice = WebCamTexture.devices[0];
				webCamTexture = new WebCamTexture(webCamDevice.name, requestedWidth, requestedHeight, requestedFPS);
			}
			else
			{
				Debug.LogError("Camera device does not exist.");
				isInitWaiting = false;
				yield break;
			}
		}

		// Starts the camera.(スレイブ時のみ）
		if (!SavingParameter.IsMaster)
		{
			webCamTexture.Play();

			while (true)
			{
				// If you want to use webcamTexture.width and webcamTexture.height on iOS, you have to wait until webcamTexture.didUpdateThisFrame == 1, otherwise these two values will be equal to 16. (http://forum.unity3d.com/threads/webcamtexture-and-error-0x0502.123922/).
#if UNITY_IOS && !UNITY_EDITOR && (UNITY_4_6_3 || UNITY_4_6_4 || UNITY_5_0_0 || UNITY_5_0_1)
                if (webCamTexture.width > 16 && webCamTexture.height > 16) {
#else
				//if (webCamTexture.didUpdateThisFrame)
				{
#if UNITY_IOS && !UNITY_EDITOR && UNITY_5_2
                    while (webCamTexture.width <= 16) {
                        webCamTexture.GetPixels32 ();
                        yield return new WaitForEndOfFrame ();
                    } 
#endif
#endif

					Debug.Log("name:" + webCamTexture.deviceName + " width:" + webCamTexture.width + " height:" + webCamTexture.height + " fps:" + webCamTexture.requestedFPS);
					Debug.Log("videoRotationAngle:" + webCamTexture.videoRotationAngle + " videoVerticallyMirrored:" + webCamTexture.videoVerticallyMirrored + " isFrongFacing:" + webCamDevice.isFrontFacing);

					isInitWaiting = false;
					hasInitDone = true;

					OnInited();

					break;
				}
				//else
				//{
				//	yield return null;
				//}
			}
		}
	}

	/// <summary>
	/// Releases all resource.
	/// </summary>
	private void Dispose()
	{
		isInitWaiting = false;
		hasInitDone = false;

		if (webCamTexture != null)
		{
			Debug.Log("Dispose Stop!");
			webCamTexture.Stop();
			WebCamTexture.Destroy(webCamTexture);
			webCamTexture = null;
		}
		if (rgbaMat != null)
		{
			rgbaMat.Dispose();
			rgbaMat = null;
		}
		if (texture != null)
		{
			Texture2D.Destroy(texture);
			texture = null;
		}
	}

	/// <summary>
	/// Raises the webcam texture initialized event.
	/// </summary>
	private void OnInited()
	{
		if (colors == null || colors.Length != webCamTexture.width * webCamTexture.height)
			colors = new Color32[webCamTexture.width * webCamTexture.height];
		if (texture == null || texture.width != webCamTexture.width || texture.height != webCamTexture.height)
			texture = new Texture2D(webCamTexture.width, webCamTexture.height, TextureFormat.RGBA32, false);

		if (rgbaMat == null)
		{
			rgbaMat = new Mat(webCamTexture.height, webCamTexture.width, CvType.CV_8UC4);
		}
		Quad.material.mainTexture = texture;
		
		Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

		

		float width = rgbaMat.width();
		float height = rgbaMat.height();

		float widthScale = (float)Screen.width / width;
		float heightScale = (float)Screen.height / height;
		if(Camera.main == null)
		{
			return;
		}
		if (widthScale < heightScale)
		{
			Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
		}
		else
		{
			Camera.main.orthographicSize = height / 2;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		// スレイブの場合所有権を保持しなければQuadを非アクティブにする.現行の仕様上スレイブ・マスター双方に互いのオブジェクトが出現するため
		if(!SavingParameter.IsMaster && !monobitView.isMine)
		{
			gameObject.SetActive(false);
		}
		// マスターの場合所有権を保持していれば非アクティブにする
		if(SavingParameter.IsMaster && monobitView.isMine)
		{
			gameObject.SetActive(false);
		}

		if (webCamTexture.isPlaying && rgbaMat != null)
		{
			// スレイブ時webCamTextureをマスターに送る
			if(!SavingParameter.IsMaster && MonobitNetwork.isConnect && MonobitNetwork.inRoom)
			{
				// webCamTextureをjpgに変換し、マスター側に飛ばす
				ConvertToJpgFile(webCamTexture.GetPixels());

				// webCamTextureをmatに変換する
				Utils.webCamTextureToMat(webCamTexture, rgbaMat, colors);
				Utils.matToTexture2D(rgbaMat, texture, colors);

			}			
		}
		if(!SavingParameter.IsMaster && rgbaMat == null)
		{
			Debug.Log("Caution! rgbaMat is lost!");
		}
		if(!webCamTexture.isPlaying)
		{
			// マスターがコネクトした瞬間にWebカメラが止まる（実機限定）ので強制再起動
			Debug.Log("WebCamera Stop!!");
			webCamTexture.Play();
		}
		// 送受信処理を確認する
		if(MonobitNetwork.isConnect && MonobitNetwork.inRoom)
		{
			DebugText.text = "Connect";
		}
		else
		{
			DebugText.text = "DisConnect";
		}
	}

	/// <summary>
	/// マスター時、テクスチャーの情報をスレイブから受信し、textureに復元する
	/// </summary>
	[MunRPC]
	void GetTextureData(byte[] texturedata)
	{
		Debug.Log("GetData!");
		Debug.Log("size:" + texturedata.Length);
		
		// textureがないなら作っておく
		if (colors == null)
		{
			colors = new Color32[requestedWidth * requestedHeight];
		}
		if (texture == null)
		{
			texture = new Texture2D(requestedWidth, requestedHeight, TextureFormat.RGBA32, false);
		}
		texture.LoadImage(texturedata);
		Debug.Log("Get LoadImage!");
		if (Quad.material == null)
		{
			return;
		}
		Quad.material.mainTexture = texture;
	}


	/// <summary>
	/// データ送信処理を行う
	/// </summary>
	/// <param name="texData"></param>
	private void ConvertToJpgFile(Color[] texData)
	{
		//// アクセスが集中しすぎるので、FPSを半分まで落とす
		//if (Send)
		//{
		//	Send = !Send;
		//	return;
		//}
		//else
		//{
		//	Send = !Send;
		//}
		Debug.Log("Send!");
		takenPhoto.SetPixels(texData);
		takenPhoto.Apply();
		byte[] jpg = takenPhoto.EncodeToJPG();
		// 自分以外に対して飛ばす
		monobitView.RPC("GetTextureData", MonobitEngine.MonobitTargets.Others, jpg);
		Debug.Log("Send OK!");
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{
		Dispose();
	}

	/// <summary>
	/// Raises the back button click event.
	/// </summary>
	public void OnBackButtonClick()
	{
		
	}

	/// <summary>
	/// Raises the play button click event.
	/// </summary>
	public void OnPlayButtonClick()
	{
		if (hasInitDone)
			webCamTexture.Play();
	}

	/// <summary>
	/// Raises the pause button click event.
	/// </summary>
	public void OnPauseButtonClick()
	{
		if (hasInitDone)
			webCamTexture.Pause();
	}

	/// <summary>
	/// Raises the stop button click event.
	/// </summary>
	public void OnStopButtonClick()
	{
		if (hasInitDone)
		{
			Debug.Log("StopButton Stop");
			webCamTexture.Stop();
		}
	}

	/// <summary>
	/// Raises the change camera button click event.
	/// </summary>
	public void OnChangeCameraButtonClick()
	{
		if (hasInitDone)
		{
			requestedDeviceName = null;
			requestedIsFrontFacing = !requestedIsFrontFacing;
			Initialize();
		}
	}
}
